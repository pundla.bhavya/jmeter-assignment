/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8642857142857143, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-20"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-19"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-27"], "isController": false}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/logout"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-25"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-26"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-23"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-24"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [0.0, 500, 1500, "https://demowebshop.tricentis.com/login-21"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-22"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-24"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-20"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-2"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-1"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-4"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-3"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-6"], "isController": false}, {"data": [0.75, 500, 1500, "https://demowebshop.tricentis.com/login-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-8"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-7"], "isController": false}, {"data": [0.5, 500, 1500, "https://demowebshop.tricentis.com/login-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-5"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-4"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-3"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-2"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-1"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-0"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-8"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-7"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-6"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-9"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-19"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-18"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-17"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-12"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-16"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-13"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-15"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-10"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-14"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/login-11"], "isController": false}, {"data": [1.0, 500, 1500, "https://demowebshop.tricentis.com/logout-13"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 69, 0, 0.0, 524.6086956521739, 182, 5210, 201.0, 1251.0, 2664.0, 5210.0, 4.104943780117794, 138.21929472306502, 7.637858624843833], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["https://demowebshop.tricentis.com/logout-23", 1, 0, 0.0, 199.0, 199, 199, 199.0, 199.0, 199.0, 199.0, 5.025125628140704, 0.6085113065326633, 4.509853957286432], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-22", 1, 0, 0.0, 182.0, 182, 182, 182.0, 182.0, 182.0, 182.0, 5.4945054945054945, 0.6653502747252747, 4.877446771978022], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-21", 1, 0, 0.0, 201.0, 201, 201, 201.0, 201.0, 201.0, 201.0, 4.975124378109452, 0.597597947761194, 4.440687189054726], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-20", 1, 0, 0.0, 185.0, 185, 185, 185.0, 185.0, 185.0, 185.0, 5.405405405405405, 0.6598395270270271, 4.7561233108108105], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-18", 1, 0, 0.0, 383.0, 383, 383, 383.0, 383.0, 383.0, 383.0, 2.6109660574412534, 5.043448107049608, 3.021479275456919], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-19", 1, 0, 0.0, 370.0, 370, 370, 370.0, 370.0, 370.0, 370.0, 2.7027027027027026, 5.954391891891892, 3.1408361486486487], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-27", 1, 0, 0.0, 195.0, 195, 195, 195.0, 195.0, 195.0, 195.0, 5.128205128205129, 19.736578525641026, 5.969551282051282], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login", 2, 0, 0.0, 4530.5, 3851, 5210, 4530.5, 5210.0, 5210.0, 5210.0, 0.1639209900827801, 92.02122584726662, 3.469874395541349], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout", 1, 0, 0.0, 1451.0, 1451, 1451, 1451.0, 1451.0, 1451.0, 1451.0, 0.6891798759476223, 26.818115739145416, 15.06302226912474], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-25", 1, 0, 0.0, 221.0, 221, 221, 221.0, 221.0, 221.0, 221.0, 4.524886877828055, 17.476491798642535, 5.302601809954751], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-26", 1, 0, 0.0, 195.0, 195, 195, 195.0, 195.0, 195.0, 195.0, 5.128205128205129, 19.73157051282051, 6.059695512820513], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-23", 1, 0, 0.0, 186.0, 186, 186, 186.0, 186.0, 186.0, 186.0, 5.376344086021506, 39.986559139784944, 6.253150201612903], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-24", 1, 0, 0.0, 182.0, 182, 182, 182.0, 182.0, 182.0, 182.0, 5.4945054945054945, 21.146334134615387, 6.4710679945054945], "isController": false}, {"data": ["Test", 1, 0, 0.0, 10512.0, 10512, 10512, 10512.0, 10512.0, 10512.0, 10512.0, 0.09512937595129375, 110.5083773306697, 6.10658131777968], "isController": true}, {"data": ["https://demowebshop.tricentis.com/login-21", 1, 0, 0.0, 3476.0, 3476, 3476, 3476.0, 3476.0, 3476.0, 3476.0, 0.28768699654775604, 123.82537174554085, 0.32898581343498273], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-22", 1, 0, 0.0, 190.0, 190, 190, 190.0, 190.0, 190.0, 190.0, 5.263157894736842, 15.758634868421053, 6.15234375], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-24", 1, 0, 0.0, 198.0, 198, 198, 198.0, 198.0, 198.0, 198.0, 5.050505050505051, 0.6165167297979798, 4.448784722222222], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-20", 1, 0, 0.0, 423.0, 423, 423, 423.0, 423.0, 423.0, 423.0, 2.3640661938534278, 128.71001403664303, 2.703438977541371], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-2", 2, 0, 0.0, 236.0, 216, 256, 236.0, 256.0, 256.0, 256.0, 0.2958579881656805, 3.717438285872781, 0.29195751664201186], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-1", 2, 0, 0.0, 1013.0, 824, 1202, 1013.0, 1202.0, 1202.0, 1202.0, 0.308546744831842, 19.95730966522678, 0.2853454759333539], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-4", 2, 0, 0.0, 753.5, 256, 1251, 753.5, 1251.0, 1251.0, 1251.0, 0.2958579881656805, 13.673897466715976, 0.2939799833579882], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-3", 2, 0, 0.0, 625.0, 260, 990, 625.0, 990.0, 990.0, 990.0, 0.29568302779420463, 3.969631274024246, 0.29597178075103486], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-6", 2, 0, 0.0, 526.5, 257, 796, 526.5, 796.0, 796.0, 796.0, 0.29581422866439877, 1.2109894985948824, 0.28454786643987573], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-5", 2, 0, 0.0, 626.5, 257, 996, 626.5, 996.0, 996.0, 996.0, 0.29581422866439877, 3.542982038529803, 0.28122573398905487], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-8", 2, 0, 0.0, 193.0, 187, 199, 193.0, 199.0, 199.0, 199.0, 0.32504469364537625, 1.2208221599219893, 0.3183787380139769], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-7", 2, 0, 0.0, 1055.0, 258, 1852, 1055.0, 1852.0, 1852.0, 1852.0, 0.315357931251971, 35.20629459358247, 0.3088906299274677], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-0", 2, 0, 0.0, 887.5, 244, 1531, 887.5, 1531.0, 1531.0, 1531.0, 0.27643400138217, 3.2435102798894264, 0.18343447650310987], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-5", 1, 0, 0.0, 201.0, 201, 201, 201.0, 201.0, 201.0, 201.0, 4.975124378109452, 0.9571284203980099, 4.411536069651741], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-4", 1, 0, 0.0, 201.0, 201, 201, 201.0, 201.0, 201.0, 201.0, 4.975124378109452, 0.9522699004975124, 4.435828669154229], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-3", 1, 0, 0.0, 200.0, 200, 200, 200.0, 200.0, 200.0, 200.0, 5.0, 0.9619140625, 4.3408203125], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-2", 1, 0, 0.0, 194.0, 194, 194, 194.0, 194.0, 194.0, 194.0, 5.154639175257732, 0.9916639819587628, 4.454937177835052], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-1", 1, 0, 0.0, 404.0, 404, 404, 404.0, 404.0, 404.0, 404.0, 2.4752475247524752, 85.29451577970296, 1.8419322400990097], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-0", 1, 0, 0.0, 227.0, 227, 227, 227.0, 227.0, 227.0, 227.0, 4.405286343612335, 2.972707874449339, 4.710731002202643], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-9", 1, 0, 0.0, 185.0, 185, 185, 185.0, 185.0, 185.0, 185.0, 5.405405405405405, 1.0346283783783783, 4.6875], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-8", 1, 0, 0.0, 190.0, 190, 190, 190.0, 190.0, 190.0, 190.0, 5.263157894736842, 1.0125411184210527, 4.507606907894737], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-7", 1, 0, 0.0, 208.0, 208, 208, 208.0, 208.0, 208.0, 208.0, 4.807692307692308, 0.9202223557692308, 4.1034405048076925], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-6", 1, 0, 0.0, 201.0, 201, 201, 201.0, 201.0, 201.0, 201.0, 4.975124378109452, 0.9571284203980099, 4.464979788557214], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-9", 2, 0, 0.0, 195.5, 186, 205, 195.5, 205.0, 205.0, 205.0, 0.3360215053763441, 0.5924676054267474, 0.3291304393481183], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-12", 1, 0, 0.0, 198.0, 198, 198, 198.0, 198.0, 198.0, 198.0, 5.050505050505051, 0.9716303661616161, 4.295888573232323], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-11", 1, 0, 0.0, 199.0, 199, 199, 199.0, 199.0, 199.0, 199.0, 5.025125628140704, 0.9667478015075376, 4.328282035175879], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-10", 1, 0, 0.0, 192.0, 192, 192, 192.0, 192.0, 192.0, 192.0, 5.208333333333333, 1.0019938151041667, 4.5013427734375], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-16", 1, 0, 0.0, 193.0, 193, 193, 193.0, 193.0, 193.0, 193.0, 5.181347150259067, 0.6173089378238342, 6.395725388601036], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-17", 1, 0, 0.0, 380.0, 380, 380, 380.0, 380.0, 380.0, 380.0, 2.631578947368421, 4.232627467105263, 3.096731085526316], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-19", 1, 0, 0.0, 203.0, 203, 203, 203.0, 203.0, 203.0, 203.0, 4.926108374384237, 0.5965209359605911, 4.3584513546798025], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-14", 1, 0, 0.0, 398.0, 398, 398, 398.0, 398.0, 398.0, 398.0, 2.512562814070352, 73.18084563442211, 2.9296875], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-18", 1, 0, 0.0, 203.0, 203, 203, 203.0, 203.0, 203.0, 203.0, 4.926108374384237, 0.952509236453202, 4.238185036945812], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-15", 1, 0, 0.0, 194.0, 194, 194, 194.0, 194.0, 194.0, 194.0, 5.154639175257732, 0.6191607603092784, 6.206709085051546], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-17", 1, 0, 0.0, 203.0, 203, 203, 203.0, 203.0, 203.0, 203.0, 4.926108374384237, 0.5965209359605911, 4.233374384236453], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-12", 2, 0, 0.0, 186.0, 184, 188, 186.0, 188.0, 188.0, 188.0, 0.3477656059815684, 1.3012342962093548, 0.3409733089897409], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-16", 1, 0, 0.0, 195.0, 195, 195, 195.0, 195.0, 195.0, 195.0, 5.128205128205129, 0.6109775641025641, 4.447115384615384], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-13", 1, 0, 0.0, 182.0, 182, 182, 182.0, 182.0, 182.0, 182.0, 5.4945054945054945, 1.0570484203296704, 6.701794299450549], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-15", 1, 0, 0.0, 193.0, 193, 193, 193.0, 193.0, 193.0, 193.0, 5.181347150259067, 0.6223688471502591, 4.336342292746114], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-10", 2, 0, 0.0, 197.5, 188, 207, 197.5, 207.0, 207.0, 207.0, 0.3359086328518643, 0.6670607469768224, 0.32885586370507225], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-14", 1, 0, 0.0, 188.0, 188, 188, 188.0, 188.0, 188.0, 188.0, 5.319148936170213, 1.0233128324468086, 4.690616688829787], "isController": false}, {"data": ["https://demowebshop.tricentis.com/login-11", 2, 0, 0.0, 193.5, 192, 195, 193.5, 195.0, 195.0, 195.0, 0.33715441672285906, 0.26817604728590694, 0.32711221974039106], "isController": false}, {"data": ["https://demowebshop.tricentis.com/logout-13", 1, 0, 0.0, 191.0, 191, 191, 191.0, 191.0, 191.0, 191.0, 5.235602094240838, 1.0072398560209423, 4.46355530104712], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 69, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
